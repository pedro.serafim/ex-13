import java.util.Scanner;

public class Main {

    private static double primeiro, segundo;

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int C;
        double N,E=0,precoHora,precoExcedente;
        boolean continuar = true;
        char texto;
        Imprime tela = new Imprime();

        do{
            E=0;
            precoHora=10;
            precoExcedente=precoHora*2;

            tela.imprime("Digite o código do operário:");
            C = entrada.nextInt();

            tela.imprime("Digite o número de horas trabalhadas:");
            N = entrada.nextDouble();

            if(N>50){
                E = N-50;
            }

            tela.imprime("\n ID: "+C+"\n Salario: "+(N*precoHora)+"\n Salario excedente: "+(E*precoExcedente)+"\n Salario total:"
                    +(E*precoExcedente+N*precoHora)+"\n\n Deseja encerrar o programa?(S/N) ");
            texto = entrada.next().charAt(0);
            if(texto == 's' || texto == 'S'){
                continuar = false;
            }

        }while(continuar);

    }

}
